﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecole
{
    interface ITel
    {
        void Telephoner(string personne);
        void Sms(string pers, string texte);
    }
}
