﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecole
{
    abstract class Personne
    {
        private string _Nom;
        private string _Prenom;


        public string Nom
        {
            get { return _Nom; }
            set { _Nom = value; }
        }


        public string Prenom
        {
            get { return _Prenom; }
            set { _Prenom = value; }
        }

        private DateTime _DateDeNaiss;

        public Personne(string nom, string prenom)
        {
            Nom = nom;
            Prenom = prenom;
        }

        public DateTime DateDeNaiss
        {
            get { return _DateDeNaiss; }
            set { _DateDeNaiss = value; }
        }

        public abstract string Matricule { get; }

        public virtual void SePresenter()
        {
            Console.WriteLine($"Je suis {Nom} {Prenom}, Matricule: {Matricule}");
        }

    }
}
