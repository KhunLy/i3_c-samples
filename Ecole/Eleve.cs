﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecole
{
    class Eleve : Personne
    {
        public Eleve(string nom, string prenom) : base(nom, prenom)
        {
        }

        public override string Matricule
        {
            get
            {
                return (Nom + Prenom).ToLower();
            }
        }

        public void Etudier(string cours)
        {
            Console.WriteLine($"J'étudie le cours de {cours}");
        }

        public void Etudier(string cours, Professeur professeur)
        {
            Console.WriteLine(
                $"J'étudie le cours de {cours} du professeur {professeur.Nom}"
            );
        }
    }
}
