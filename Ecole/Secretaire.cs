﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecole
{
    class Secretaire
    {
        ITel tel;

        public Secretaire(ITel tel)
        {
            this.tel = tel;
        }

        public void AppelerSonPatron()
        {
            tel.Telephoner("Son Patron");
        }
    }
}
