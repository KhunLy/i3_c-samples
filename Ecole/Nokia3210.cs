﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecole
{
    class Nokia3210 : ITel
    {
        public void Sms(string pers, string texte)
        {
            Console.WriteLine($"J'envoie le texte {texte} à {pers} depuis mon nokia");
        }

        public void Telephoner(string personne)
        {
            Console.WriteLine($"Je tel à {personne} depuis mon nokia");
        }

        public void JouerAuSnake()
        {
            Console.WriteLine("Je joue au snake");
        }
    }
}
