﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecole
{
    class Professeur: Personne
    {
        public Professeur(string nom, string prenom) : base(nom, prenom)
        {
        }

        public override string Matricule
        {
            get { return (Prenom + Nom).ToUpper(); }
        }
    }
}
